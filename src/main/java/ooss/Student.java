package ooss;

public class Student extends Person implements Observer{

    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce(){
        String introduction = super.introduce().concat(" I am a student. ");
        if (this.getKlass() != null){
            if (this.getKlass().isLeader(this)){
                String leaderIntro = "I am the leader of class " + this.getKlass().getNumber() + ".";
                introduction += leaderIntro;
            }else{
                String nonLeaderIntro = "I am in class " + this.getKlass().getNumber();
                introduction += introduction.concat(nonLeaderIntro);
            }
        }

        return introduction;
    }

    public void join(Klass klass){
        this.setKlass(klass);
    }

    public Klass getKlass() {
        return klass;
    }

    private Klass setKlass(Klass klass) {
        return this.klass = klass;
    }

    public boolean isIn(Klass klass){
        return this.getKlass() == klass;
    }

    @Override
    public void onLeaderAssigned(Student leader, Klass klass) {
        if (this != leader && this.isIn(klass)) {
            System.out.println("I am " + this.getName() + ", student of class " + klass.getNumber() + ". I know " + leader.getName() + " become Leader.");
        }
    }
}
