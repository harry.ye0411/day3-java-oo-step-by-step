package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person implements Observer {
    private ArrayList<Klass> klasses;

    public Teacher(int id, String name, int age){
        super(id, name, age);
        klasses = new ArrayList<Klass>();
    }
    @Override
    public String introduce(){
        if (getKlasses().size() > 0){
            return (super.introduce() +" I am a teacher." + " I teach Class " + klasses.stream()
                    .map(Klass::getNumber)
                    .map(String::valueOf)
                    .collect(Collectors.joining(", "))+ '.');
        }
        return (super.introduce() +" I am a teacher.");
    }

    public ArrayList<Klass> getKlasses() {
        return klasses;
    }

    public void assignTo(Klass klass){
        this.klasses.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return this.getKlasses().contains(klass);
    }

    public boolean isTeaching(Student student){
        return getKlasses().contains(student.getKlass());
    }

    @Override
    public void onLeaderAssigned(Student leader, Klass klass) {
        if (belongsTo(klass)) {
            System.out.println("I am " + this.getName() + ", teacher of class " + klass.getNumber() + ". I know " + leader.getName() + " become Leader.");
        }
    }
}
