package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {
    private final int number;
//    private Student[] students;
    private Student leader;
    private final List<Observer>  observers = new ArrayList<Observer>();
    private List<Object> objects = new ArrayList<>();

    public Klass(int number){
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    public int getNumber() {
        return number;
    }

    public Student getLeader() {
        return leader;
    }

    public void setLeader(Student leader) {
        this.leader = leader;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public boolean isLeader(Student student){
        return this.getLeader() == student;
     }

    public void attach(Object o) {
        objects.add(o);
    }

    public void assignLeader(Student leader){
        if (leader.isIn(this)){
            this.setLeader(leader);

            /* Notify attached objects */
            for (Object o : objects) {
                if (o instanceof Teacher) {
                    Teacher teacher = (Teacher) o;
                    if (teacher.belongsTo(this)) {
                        System.out.println("I am " + teacher.getName() + ", teacher of Class " + this.getNumber() + ". I know " + leader.getName() + " become Leader.");
                    }
                } else if (o instanceof Student) {
                    Student student = (Student) o;
                    if (student != leader && student.isIn(this)) {
                        System.out.println("I am " + student.getName() + ", student of Class " + this.getNumber() + ". I know " + leader.getName() + " become Leader.");
                    }
                }
            }
        } else {
            System.out.println("It is not one of us.");
        }
    }
}
